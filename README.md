# README #

This is the first (_and so far only_) project I have made using Asp.Net MVC

### What is It?###

It's not much but a __simple school gradebook__

### What does It do? ###

* It uses an MSQL database
* It allows the teacher to get a detailed desciription of his class
* To asynchornically add grades using ajax, also load multiple elements the Ajax way.
* Gives students ability to check all of his grades, subjects, averages, statistic and so on
* You can edit user parameters too sometimes

### Why did you make it? ###

School assignment to write a simple app in _.NET web forms_.
I decided using MVC would be cooler because it looks a bit like symfony so what the heck

### Is it flawless? ###

__Oh God forbid no!__ 
The project has many (mostly form ~~laziness~~ safety related) flaws.
I didn't have time to handle tokens in ajax due to a lack of time.
Many queries and Models could have been written better, same accounts for db table names
as a result of _getting new cool ideas_ while having the table fully function

### Will it be further developed? ###
# NO #

