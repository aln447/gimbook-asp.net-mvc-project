﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using gimBook.Models;
using gimBook.ViewModels;
using gimBook.ViewModels.SmallView;

namespace gimBook.ViewModels
{
    public class StudentSubjectViewModel
    {
        public List<tableGrades> Grades { get; set; }
        public List<tableAverages> Averages { get; set; }
    }
}