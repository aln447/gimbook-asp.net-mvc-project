﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using gimBook.ViewModels.SmallView;

namespace gimBook.ViewModels.SmallView
{
    public class StudentListViewModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }

        public List<tableGrades> Grades;

    }
}