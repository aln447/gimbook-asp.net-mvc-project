﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gimBook.ViewModels.SmallView
{
    public class tableGrades
    {
        public System.DateTime Date;

        public string DateStr;

        public int Grade;

        public string Subject;

        public int Mult;

        public string SubjectName;

        public tableGrades()
        {
            //Nullowanie niektórych elementów które mogą stać się NULL
            //Tak jakby ktoś to kiedyś czytał.....
            Mult = 0;
            SubjectName = "";
        }
    }
}