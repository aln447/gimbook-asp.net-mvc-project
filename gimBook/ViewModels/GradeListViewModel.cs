﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gimBook.Models
{
    public class GradeListViewModel
    {
        public string[] Names = new string[7];

        public GradeListViewModel()
        {
            //Ustawienie pierwszych indeksów na zero pomaga uniknąć problemów przy dodawaniu wartości
            Names[0] = "";
            Names[1] = "Niedostateczny";
            Names[2] = "Dopuszczający";
            Names[3] = "Dostateczny";
            Names[4] = "Dobry";
            Names[5] = "BardzoDobry";
            Names[6] = "Celujący";
        }
    }
}