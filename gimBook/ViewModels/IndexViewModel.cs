﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using gimBook.Models;
using gimBook.ViewModels.SmallView;

namespace gimBook.ViewModels
{
    public class IndexViewModel
    {

        /*
         * Parametry dzielone między użytkownikami
         * * Dane Konta
         * * Klasa użytkownika lub podlegająca
         * * Średnia klasy użytkownika lub podlegającej
         * * Lista Przedmiotów użytkownika
         */

        public AspNetUser CurrentUser { get; set; }
        public @class UserClass { get; set; }

        public double UserClassAverage;
        public List<TwoStringIntViewModel> SubjectList { get; set; }

        /*
         * Parametry Ucznia
         * * Trzy ostatnie OCENY wraz z parametrami PRZEDMIOTU
         * * Średnia użytkownika ogólna
         * * Przedmiot ze średnią największą i najmniejszą
         * * Wszystkie oceny
         */

        public List<TwoStringIntViewModel> LastThreeGrades { get; set; }

        public double UserAverage;
        public string BestClass;
        public string WorstClass;
   
        public List<GradeNumViewModel> AllGradesNum;

        /*
         * Parametry nauczyciela
         * * Lista Uczniów klasy nauczyciela
         */
        public List<StudentListViewModel> StudentList;
        /*
         * Lista Nazw przedmiotów do użytku w kodzie
         */ 
        public GradeListViewModel GradeList;

        /*
         * Konstruktor: 
         * Ustawi wszystkie Nullable wartości, na wypadek ustawień nauczyciela
         */
         public IndexViewModel ()
        {
            CurrentUser = null;
            UserClass = null;
            UserClassAverage = 0;
            SubjectList = null;

            LastThreeGrades = null;
            UserAverage = 0;
            BestClass = "";
            WorstClass = "";
            AllGradesNum  = null;

            StudentList = null;
        }

        
    }
}
 