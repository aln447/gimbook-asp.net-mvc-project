﻿/*
 * 
 * Skrypt jQuery dla GimBook
 * 
 *      Index:
 *          currentSubjectId -> Przechowuje zmienną globalną aktualnej klasy dla widoku nauczyciela
 *          sendGradesUrl -> Przechowuje poprawną ścieżkę do controlera ocen
 *          getGradeName -> Przechowuje tabele nazw ocen, zwraca ocene o danym indeksie
 *          getSubject -> AJAXowa funkcja tworząca tabele ocen danego przedmiotu studenta
 *          getSubjectStudents -> AJAXowa funkcja zwracająca użytkowników danego przedmiotu
 *          addGrades -> To jest funkcja zajmująca się wysyłaniem ocen do kontrolera
 *          $(document).ready() -> Zajmuje się ogarnianiem przycisków
 * 
 * Alan Krygowski
 */


currentSubjectId = 0;

sendGradesUrl;

function getGradeName(index)
{
    var gradeList = [
        '',
        'Niedostateczny',
        'Dopuszczający',
        'Dostateczny',
        'Dobry',
        'Bardzo Dobry',
        'Celujący'
    ];

    return gradeList[index];
}

function getSubject(classid)
{
    
   $.post("Ajax/GetStudentSubject",
   {
      id: classid
   },
   function (data) {

       var html = "";

       //Sprawdź czy tablica nie jest pusta
       if (data['Grades'].length > 0) {
           

           //Dodaj podstawowe elementy góry tabeli
           html += (
               "<div class='col-lg-8' >"
                + "<p><h1 id='current-grade-name'>" + $("#subject-button-name-" + classid).text() + "</h1></p>"
               + "<table class='table table-striped' style='font-size: 20px'>"
               + "<thead><tr><th>Data</th><th>Ocena</th><th>Temat</th><th>Waga</th></tr></thead><tbody>"
               );

           //Dodaj pętle Nadającą jej wszystkie wartości
           for (var i = 0; i < data['Grades'].length; i++) {
               html += (
                   "<tr> <td>"
                   + data['Grades'][i]['DateStr'] + "</td><td>"
                   + getGradeName(data['Grades'][i]['Grade']) + "</td><td>"
                   + data['Grades'][i]['Subject'] + "</td><td>"
                   + data['Grades'][i]['Mult'] + "</td> </tr>"
                   );
           }

           //Dodaj końcówkę tabeli
           html += ("</tr></tbody></table></div>");


           //Dodaj panel boczny
           html += (
               '<div class="col-lg-4">'
                + '<h1>Twoja średnia</h1>'
                + '<p class="text-center very-big-text">' + (data['Averages'][0]['Value']).toFixed(2) + '</p>'
                + '<h2>Statystyka przedmiotu</h2>'
                + '<p><div id="statistic-student"></div></p>'
                + '</div>'
           );


           //ODPAL GRAF SYNU
           var chartSeries = [0];
           var chartLabels = ["Start"];
           for (var i = 0; i < data['Averages'].length; i++) {
               chartSeries.push(data['Averages'][data['Averages'].length - i - 1]['Value']);
               chartLabels.push(data['Averages'][data['Averages'].length - i - 1]['DateStr'])
           }
           console.log("Labels:" + chartLabels);
           console.log("Series:" + chartSeries);
           setTimeout(function () {
               new Chartist.Line('#statistic-student', {
                   labels: chartLabels,
                   series: [
                     chartSeries,
                   ]
               }, {
                   showArea: true,
                   fullWidth: true,
                   chartPadding: {
                       right: 40
                   }
               });
           }, 50);
          

       } else {
           html = "<h2 class='text-center text-danger'>Ten przedmiot niema jeszcze ocen :) </h2>";
       }
       
       $("#current-subject").html(html);
   });


}

function getSubjectStudents(subjId)
{
    currentSubjectId = subjId;

    $.post(
    "Ajax/TeacherTable",
    {
        id: subjId
    },
    function (data) {

        var html = "";

        if (data.length > 0) {

            html += ('<table class="table table-striped" style="font-size: 20px" id="student-table">'
                + '<thead> <tr><th>Imię</th><th>Nazwisko</th><th>Ocena</th><th>Waga</th><th>Temat</th></tr></thead><tbody>'
                );

            for (var i = 0; i < data.length; i++) {

                html += (
                    '<tr id=' + data[i]['Id'] + ' class="student-grade-data-row">'
                        + '<td class="first-name">' + data[i]['FirstName'] + '</td>'
                        + '<td class="last-name">' + data[i]['LastName'] + '</td>'
                        + '<td class="grade">'
                           + '<select class="form-control btn-primary grade-select">'
                                + '<option value="0">-- BRAK --</option>'
                                + '<option value="6">Celujący</option>'
                                + '<option value="5">B.Dobry</option>'
                                + '<option value="4">Dobry</option>'
                                + '<option value="3">Dostateczny</option>'
                                + '<option value="2">Dopusczający</option>'
                                + '<option value="1">Niedostateczny</option>'
                            + '</select>'
                        + '</td>'
                        + '<td class="mult">'
                            + '<select class="form-control btn-primary mult-select">'
                                + '<option value="4">IV</option>'
                                + '<option value="3">III</option>'
                                + '<option value="2">II</option>'
                                + '<option value="1">I</option>'
                            + '</select>'
                        + '</td>'
                        + '<td><input type="text" name="name" value="" class="form-control topic-input" placeholder="Sprawdzań/Karktówka" /></td>'
                    + '</tr>'
                    );

            }//table loop

            html += '</tbody></table><p class="text-center"><button class="btn btn-danger btn-lg" onClick="addGrades()">DODAJ OCENY</button></p></form>';

        } //data length > 0
        else {
            html = '<h1 class="text-center text-danger">Ten przedmiot niema studentów ._.</h1>';
        }

        $("#student-list-grade-form").html(html);
    }
);
}

function addGrades()
{
    /*
     
            ============================================================
            To jest funkcja zajmująca się wysyłaniem ocen do kontrolera.

            Ta funkcja:
            -- Tworzy zmienną json do której po kolei pakuje wszystkie
               rzędy osób danej klasy. 
                -- Zmienna powinna przypominać klasę "grade" --
                -- Każdy rząd poiwnien mieć wypełnione pole "subjectId"
            -- Filtruje tablice:
                -- Wyrzuca wszystkie wartości z zerem w polu oceny --
                -- Przeszukuje czy każde pole posiada opis -- 
                -- W razie jakichkolwiek braków informuje o tym
                   oraz podaje dokładną lokalizacje błędu --
            -- Wysyła wszystko od odpowiedniego kontrolera
            -- Zwraca odpowiednie błędy i komunikaty

     
     */

    //Tworzenie zmiennej
    var data = { "Model": [] };

    //Populowanie
    $(".student-grade-data-row").each(function () {
        data.Model.push({
            'subjectId' : currentSubjectId,
            'studentId': $(this).attr('id'),
            'grade1' : $(this).find(".grade-select").val(),
            'mult' : $(this).find(".mult-select").val(),
            'subject': $(this).find(".topic-input").val()
        });
    });

    //Filtrowanie
    var modelLength = data.Model.length;
    for (var i = 0; i < modelLength; i++) {
        if (data.Model[i].grade1 == 0) {
            data.Model.splice(i, 1);
            modelLength--;
            i--;
        }
        else if (data.Model[i].subject == "") {
            alert("Brak tematu oceny " + getGradeName(data.Model[i].grade1));
        }
    }

    console.log("Wysyłam do: " + sendGradesUrl);
    
    //Wysyłanie i pobieranie błędu 
    $.ajax({
        type: "POST",
        url: '/Ajax/SendGrades',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        success: function (dt) {
            alert(dt);
            $(".grade-select").val(0);
            $(".mult-select").val(0);
            $(".topic-input").val("");
        }
    });
}

$(document).ready(function () {

    $('#subject-picker').change(function () {

        if ($("#subject-picker").val() == "default")
            return false;

        $("#student-list-top").show();
        getSubjectStudents($("#subject-picker").val());

    });

    $("#all-grades-set").change(function () {
        $(".grade-select").val($(this).val());
    });

    $("#all-mult-set").change(function () {
        $(".mult-select").val($(this).val());
    });

    $("#all-topic-set").keyup(function () {
        $(".topic-input").val($(this).val());
    });

    $(".grades-button").on("click", function () {
        var $table = $(this).prev(".list-grade-table");
        if ($table.is(":visible")) {
            $table.fadeOut();
            $(this).text("Pokaż dziennik...")
        } else {
            $table.fadeIn();
            $(this).text("Ukryj dziennik...")
        }
    });
});
