﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using gimBook.Models;
using gimBook.ViewModels;
using gimBook.ViewModels.SmallView;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;

namespace gimBook.Controllers
{
    public class AjaxController : Controller
    {

        private DataBase Db = new DataBase();


        // GET: Ajax

        /*


            Metody AJAXowe
            Kontrolery służące zwracaniu danych asynchronicznych     

            1. GetSubject
            Na podstawie id będącego id przedmiotu znajduje listę ocen dla zalogowanego użytkownika.
            Dodatkowo pobiera listę średnich użytkownika z danego przedmiotu.


            TODO: SPRAWDŹ CZY TRZEBA CHILDONLY CZY KI CHUJ

        */


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetStudentSubject(int id)
        {


            string userID = User.Identity.GetUserId();

            StudentSubjectViewModel Model = new StudentSubjectViewModel();


            Model.Grades = (
               from g in Db.grades
               where (g.subjectId == id)
               && (g.studentId == userID)
               orderby g.date descending
               select new tableGrades
               {
                   Date = g.date,
                   Grade = g.grade1,
                   Subject = g.subject,
                   Mult = g.mult
               }
               ).ToList();



            Model.Averages = (
                from a in Db.averages
                where (a.subjectId == id)
                && (a.studentId == userID)
                orderby a.date descending
                select new tableAverages
                {
                    Date = a.date,
                    Value = a.value
                }
                ).ToList();


            foreach (tableGrades grade in Model.Grades)
            {
                grade.DateStr = grade.Date.ToString("dd/MM/yy  HH:mm");
            }
            foreach (tableAverages average in Model.Averages)
            {
                average.DateStr = average.Date.ToString("dd/MM");
            }

            return Json(new {
                Grades = Model.Grades,
                Averages = Model.Averages
            }, JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult TeacherTable(int id)
        {
            List<tableTeacherTableRow> Model = new List<tableTeacherTableRow>();

            Model = (
                from u in Db.AspNetUsers
                join c in Db.classes on u.ClassId equals c.id
                join s in Db.subjects on c.id equals s.classId
                where s.id == id
                select new tableTeacherTableRow
                {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Id = u.Id
                }).ToList();

            return Json(Model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendGrades(List<grade> Model)
        {

            //1. Zapisz nową ocenę do bazy
            foreach (grade g in Model)
            {
                /* Data ustalana jest w momencie dodawania do bazy */
                g.date = DateTime.Now;
                Db.grades.Add(g);
            }
            Db.SaveChanges();

            // Zapisz nowe średnie studentów do bazy.
            average newAv = new average();
            foreach (grade g in Model)
            {
                newAv.studentId = g.studentId;
                newAv.subjectId = g.subjectId;
                newAv.date = DateTime.Now;
                newAv.value = (
                    from gr in Db.grades
                    where gr.studentId == g.studentId
                    && gr.subjectId == g.subjectId
                    select (gr.grade1)
                    ).Average();

                Db.averages.Add(newAv);
                Db.SaveChanges();
            }
            
            /* Save changes musi być wywołane dwa razy aby brało pod uwagę średnią PO dodaniu ocen */


            return Content("Sukces!!");
        }
    }
}