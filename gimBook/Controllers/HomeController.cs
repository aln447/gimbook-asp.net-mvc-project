﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using gimBook.Models;
using gimBook.ViewModels;
using gimBook.ViewModels.SmallView;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace gimBook.Controllers
{



    public class HomeController : Controller
    {
        
        private DataBase Db = new DataBase();

        public ActionResult Index()
        {

            

            /*

                   
                   Główny kontroler wartości wyświetlanych:
                        1. Sprawdź czy ktoś jest zalogowany
                        2. Jeśli jest, przypisz wartości wspólne
                            -- CurrentUser --
                            -- UserClass --
                            -- UserClassAverage --
                            -- SubjectList --
                        3. Na podstawie ClassID sprawdź czy to student, jeśli tak, przypisz wartości specyficzne
                            -- UserClass
                            -- LastThreeGrades --
                            -- UserAverage --
                            -- BestClass --
                            -- WorstClass --
                            -- AllGradesNum --
                        4. Zrobi inne rzeczy których nie chciało mi się tu opisywać, elo

 
             */

            if (Request.IsAuthenticated)
            {
                ViewModels.IndexViewModel Model = new ViewModels.IndexViewModel();

                // Current User
                string userID = User.Identity.GetUserId();
                
                Model.CurrentUser = Db.AspNetUsers.Single(g => g.Id == userID);
                int userClassId = Model.CurrentUser.ClassId;




                //  User Class Average
                /*  
                   
                    Query pobiera na podstawie klasy użytkownika 
                    (klasa zerowa nie bedzie posiadała ocen)
                    Lub na podstawie ocen klas których nauczycielem
                    jest uzytkownik


                */


                Model.UserClassAverage = (
                from a in Db.averages
                join s in Db.subjects on a.subjectId equals s.id
                join c in Db.classes on s.classId equals c.id
                where (c.id == userClassId)
                || (c.classTeacherId == userID)
                select (double?) a.value 
                ).Average() ?? 0;
                
                


                //Sprawdź czy jest to uczeń
                if(userClassId != 0)
                {
                    // User Class 1
                    Model.UserClass = this.Db.classes.Single(g => g.id == userClassId);

                    // Subject List 1
                    /*

                        Query pobiera albo wszystkie przedmioty ucznia
                        na podstawie połączenia przedmiot - klasa - uczeń

                     */
                    Model.SubjectList = (
                        from s in Db.subjects
                        join c in Db.classes on s.classId equals c.id
                        join u in Db.AspNetUsers on s.teacherId equals u.Id
                        where c.id == userClassId
                        select new TwoStringIntViewModel
                        {
                            Int = s.id,
                            Str1 = s.subjectName,
                            Str2 = u.LastName
                        }
                        ).ToList();



                    // LastThreeGrades (with subject names)
                    Model.LastThreeGrades = (
                           from g in Db.grades
                           join s in Db.subjects on g.subjectId equals s.id
                           where (g.studentId == userID)
                           orderby g.date descending
                           select new TwoStringIntViewModel
                           {
                               Int = g.grade1,
                               Str1 = s.subjectName,
                               Str2 = g.subject
                           }
                    ).Take(3).ToList();

                    // UserAverage
                    Model.UserAverage = (
                           from a in Db.averages
                           where (a.studentId == userID)
                           select (double?) a.value
                       ).Average() ?? 0;

                    // BestClass
                    Model.BestClass = (
                        from s in Db.subjects
                        join a in Db.averages on s.id equals a.subjectId
                        where a.studentId == userID
                        orderby a.value descending
                        select s.subjectName
                       ).FirstOrDefault();

                    // WorstClass
                    Model.WorstClass = (
                        from s in Db.subjects
                        join a in Db.averages on s.id equals a.subjectId
                        where a.studentId == userID
                        orderby a.value ascending
                        select s.subjectName
                       ).FirstOrDefault();

                    // AllGradesNumString
                    Model.AllGradesNum = (
                        from g in Db.grades
                        where g.studentId == userID
                        group g by g.grade1 into c
                        select new GradeNumViewModel
                            {
                                Grade = c.Key,
                                Count = c.Count()
                            }
                        ).ToList();

                }
                else // To jest Nauczyciel
                {

                    // User Class 1
                    Model.UserClass = this.Db.classes.Single(g => g.classTeacherId == userID);

                    // Subject List 2
                    /*

                        Jeśli jest to nauczyciel pobieramy parametry klasy miast 
                        Nazwiska nauczyciela

                     */
                    Model.SubjectList = (
                        from s in Db.subjects
                        join c in Db.classes on s.classId equals c.id
                        join u in Db.AspNetUsers on s.teacherId equals u.Id
                        where s.teacherId == userID
                        select new TwoStringIntViewModel
                        {
                            Int = s.id,
                            Str1 = s.subjectName,
                            Str2 = (c.classYear + " " + c.className)
                        }
                        ).ToList();

                    /*
                     * Lista studentów klasy
                     * Pobieramy tutaj tylko samą tablice studentów ale też całe ich dzienniki
                     * Przez to potrzebujemy dodatkowego obiektu zagniezdzonego
                     * W skrócie: New level obiektowość!
                     * 
                     * ps. Jeszcze w tym wewn. zapytaniu musimy joinować, to już w ogóle wow
                     */

                    Model.StudentList = (
                        from u in Db.AspNetUsers
                        where u.ClassId == Model.UserClass.id
                        select new StudentListViewModel
                        {
                            Id = u.Id,
                            FirstName = u.FirstName,
                            LastName = u.LastName,
                            EMail = u.Email,
                            Grades = (
                                from g in Db.grades //Z Ocen
                                join s in Db.subjects on g.subjectId equals s.id
                                where g.studentId == u.Id
                                orderby g.date descending
                                select new tableGrades
                                {
                                    Grade = g.grade1,
                                    Subject = g.subject,
                                    SubjectName = s.subjectName,
                                }
                            ).ToList()
                        }
                        ).ToList();
                    
                    
                }

                Model.GradeList = new GradeListViewModel();

                return View(Model);
            }
            else
            {
                // Jeśli nikt nie jest zalogowany, strona odsyła do strony logowania
                return RedirectToAction("Login", "Account");

            }
            

            //studentIndex.ClassList = this.Db.classes.ToList();

            

            
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        /*
         * Edycja Danych Studenta Przez Nauczyciela.
         * Dzieje się requestem Post
         * Tak dla zabawy, why not
         */

        public ActionResult EditStudent(string id)
        {
            //Tylko Nauczyciel Może zmieniać rekordy
            //Więc pobieramy jego klase
            string userID = User.Identity.GetUserId();
            int userClass = (
                from u in Db.AspNetUsers
                where u.Id == userID
                select u.ClassId
                ).FirstOrDefault();

            //I sprawdzamy czy sie zgadza
            if (userClass == 0)
            {
                StudentListViewModel Student = new StudentListViewModel();

                //Tutaj damy formularz z danymi studenta o określonym Id
                Student = (
                    from u in Db.AspNetUsers
                    where u.Id == id
                    select new StudentListViewModel
                    {
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        EMail = u.Email,
                        Id = id
                    }
                    ).Single();

                return View(Student);
            }
            return Content("Nie jesteś uprzywilejowany do tego, sorry");
        }

        [HttpPost]
        public ActionResult EditStudent(StudentListViewModel NewUser)
        {
            //Tutaj zmienimy wpis bazy dodając
            AspNetUser OldUser = new AspNetUser();

            //Wczytamy Old uzytkownika
            OldUser = (
                from u in Db.AspNetUsers
                where u.Id == NewUser.Id
                select u
                ).FirstOrDefault();

            //Sprawdzania które pola sie różnią i jeśli tak, zmienianie ich
            if (OldUser.FirstName != NewUser.FirstName)
                OldUser.FirstName = NewUser.FirstName;

            if (OldUser.LastName != NewUser.LastName)
                OldUser.LastName = NewUser.LastName;

            if (OldUser.Email != NewUser.EMail) {
                OldUser.Email = NewUser.EMail;
                OldUser.UserName = NewUser.EMail;
            }
                

            //Zapisz do bazy danych
            Db.SaveChanges();

            //Odeślij użytkownika do strony głównej
            return RedirectToAction("Index", "Home");
        }

    }
}