﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gimBook.Models
{
    public class Averages
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public int StudentId { get; set; }
        public int Value { get; set; }
        public int Date { get; set; }
    }
}