﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gimBook.Models
{
    public class Grades
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public int StudentId { get; set; }
        public int Grade { get; set; }
        public int Mult { get; set; }
        public string Subject { get; set; }
        public int Date { get; set; }
    }
}