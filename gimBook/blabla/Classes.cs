﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gimBook.Models
{
    public class Classes
    {
        public int classId { get; set; }
        public string className { get; set; }
        public int classYear { get; set; }
        public int classTeacherId { get; set; }
    }
}